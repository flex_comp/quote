package quote

import (
	"fmt"
	"gitlab.com/flex_comp/comp"
	"gitlab.com/flex_comp/log"
	"math/rand"
	"testing"
	"time"
)

type ExchangeQuote struct {
	ch chan *Item
}

func (e *ExchangeQuote) Name() string {
	return "exchange_test"
}

func (e *ExchangeQuote) Run(i ...interface{}) chan *Item {
	fmt.Println(append([]interface{}{"config:"}, i...)...)
	e.ch = make(chan *Item)
	return e.ch
}

func (e *ExchangeQuote) Sub(s string) error {
	go func() {
		for {
			<-time.After(time.Millisecond * time.Duration(rand.Intn(1000-1)+1))
			e.ch <- &Item{
				Name:   e.Name(),
				Symbol: s,
				Price:  2.0 + rand.Float64()*(3.0-2.0),
				Time:   time.Now(),
			}
		}
	}()
	return nil
}

func (e *ExchangeQuote) UnSub(s string) error {
	return nil
}

func TestQuotes(t *testing.T) {
	t.Run("quotes", func(t *testing.T) {
		Reg(new(ExchangeQuote))
		_ = comp.Init(map[string]interface{}{
			"config": "./test/server.json",
		})

		go func() {
			<-time.After(time.Second)
			chAgg, _, _ := NewAgg("exchange_test", "adausdt")
			chTick, _, _ := NewTick("exchange_test", "adausdt")
			for {
				select {
				case item := <-chAgg:
					log.Info("新归集数据:\r\n", item)
				case item := <-chTick:
					_ = item
					//log.Info("Tick:", time.Now().Format(time.StampMilli), " ", item)
				}
			}
		}()

		go func() {
			<-time.After(time.Second * 50)
			now := time.Now()
			t, e := SubTimeline("exchange_test", "adausdt", now.Add(-time.Second*40), now)
			log.Info("指定分段:", now.Add(-time.Second*2).Format(time.StampMilli), " - ", now.Format(time.StampMilli),
				"\r\n进: 时-", t.In.Time.Format(time.StampMilli), " 价-", t.In.Price, "\r\n",
				"出: 时-", t.Out.Time.Format(time.StampMilli), " 价-", t.Out.Price, "\r\n",
				"高: 时-", t.High.Time.Format(time.StampMilli), " 价-", t.High.Price, "\r\n",
				"低: 时-", t.Low.Time.Format(time.StampMilli), " 价-", t.Low.Price, "\r\n",
				e)
			ts, e := TimeLine("exchange_test", "adausdt", now.Add(-time.Second*40), now, time.Second*5)
			for _, items := range ts {
				log.Info("指定多分段:", now.Add(-time.Second*2).Format(time.StampMilli), " - ", now.Format(time.StampMilli),
					"\r\n进: 时-", items.In.Time.Format(time.StampMilli), " 价-", items.In.Price, "\r\n",
					"出: 时-", items.Out.Time.Format(time.StampMilli), " 价-", items.Out.Price, "\r\n",
					"高: 时-", items.High.Time.Format(time.StampMilli), " 价-", items.High.Price, "\r\n",
					"低: 时-", items.Low.Time.Format(time.StampMilli), " 价-", items.Low.Price, "\r\n",
					e)
			}
		}()

		done := make(chan bool)
		go func() {
			<-time.After(time.Minute)
			done <- true
		}()

		_ = comp.Start(done)
	})
}
