package quote

import (
	"sync"
	"time"
)

func newManager(exchangeName string, quote Quote, args ...interface{}) *manager {
	m := &manager{
		name:    exchangeName,
		args:    args,
		quote:   quote,
		symbols: make(map[string]*symbol),
	}

	go m.run()
	return m
}

type manager struct {
	name  string
	args  []interface{}
	quote Quote

	symbols    map[string]*symbol
	mtxSymbols sync.RWMutex
}

func (m *manager) newTick(symbol string) (chan *Item, uint64, error) {
	m.mtxSymbols.Lock()
	defer m.mtxSymbols.Unlock()

	s, ok := m.symbols[symbol]
	if !ok {
		s = newSymbol(m.name, symbol)
		err := m.quote.Sub(symbol)
		if err != nil {
			return nil, 0, err
		}

		m.symbols[symbol] = s
	}

	return s.newTick()
}

func (m *manager) delTick(symbol string, id uint64) {
	m.mtxSymbols.Lock()
	defer m.mtxSymbols.Unlock()

	s, ok := m.symbols[symbol]
	if !ok {
		return
	}

	s.delTick(id)
}

func (m *manager) newAgg(symbol string) (chan *Item, uint64, error) {
	m.mtxSymbols.Lock()
	defer m.mtxSymbols.Unlock()

	s, ok := m.symbols[symbol]
	if !ok {
		s = newSymbol(m.name, symbol)
		err := m.quote.Sub(symbol)
		if err != nil {
			return nil, 0, err
		}

		m.symbols[symbol] = s
	}

	return s.newAgg()
}

func (m *manager) delAgg(symbol string, id uint64) {
	m.mtxSymbols.Lock()
	defer m.mtxSymbols.Unlock()

	s, ok := m.symbols[symbol]
	if !ok {
		return
	}

	s.delAgg(id)
}

func (m *manager) subTimeline(symbol string, from time.Time, to time.Time) (*Items, error) {
	m.mtxSymbols.RLock()
	defer m.mtxSymbols.RUnlock()

	s, ok := m.symbols[symbol]
	if !ok {
		return nil, ErrNotSubscribeSymbol
	}

	return s.subTimeline(from, to)
}

func (m *manager) timeline(symbol string, from time.Time, to time.Time, interval time.Duration) ([]*Items, error) {
	m.mtxSymbols.RLock()
	defer m.mtxSymbols.RUnlock()

	s, ok := m.symbols[symbol]
	if !ok {
		return nil, ErrNotSubscribeSymbol
	}

	return s.timeline(from, to, interval)
}

func (m *manager) latestItem(symbol string) (*Item, error) {
	m.mtxSymbols.RLock()
	defer m.mtxSymbols.RUnlock()

	s, ok := m.symbols[symbol]
	if !ok {
		return nil, ErrNotSubscribeSymbol
	}

	return s.latestItem()
}

func (m *manager) run() {
	ch := m.quote.Run(m.args...)
	for {
		select {
		case item := <-ch:
			item.Name = m.name
			m.mtxSymbols.RLock()
			s, ok := m.symbols[item.Symbol]
			if ok {
				s.newQuote(item)
			}
			m.mtxSymbols.RUnlock()
		}
	}
}
