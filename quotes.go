package quote

import (
	"encoding/json"
	"gitlab.com/flex_comp/comp"
	"gitlab.com/flex_comp/conf"
	"gitlab.com/flex_comp/log"
	"gitlab.com/flex_comp/util"
	"io/ioutil"
	"os"
	"time"
)

var (
	quotes *Quotes
)

func init() {
	quotes = &Quotes{
		lib: make(map[string]Quote),
		mgr: make(map[string]*manager),
	}

	_ = comp.RegComp(quotes)
}

type Quotes struct {
	lib map[string]Quote
	mgr map[string]*manager
}

func (q *Quotes) Init(map[string]interface{}, ...interface{}) error {
	path := util.ToString(conf.Get("exchange"))
	if len(path) == 0 {
		return ErrNotFoundConf
	}

	srcFile, e := os.OpenFile(path, os.O_RDWR, 0666)
	if e != nil {
		return e
	}

	defer srcFile.Close()
	raw, e := ioutil.ReadAll(srcFile)
	if e != nil {
		return e
	}

	config := make(map[string][]interface{})
	e = json.Unmarshal(raw, &config)
	if e != nil {
		return e
	}

	<-time.After(time.Second * 2)
	for name, args := range config {
		impl, ok := q.lib[name]
		if !ok {
			log.Warn("未知的交易所:", name)
			continue
		}

		q.mgr[name] = newManager(name, impl, args...)
	}

	return nil
}

func (q *Quotes) Start(...interface{}) error {
	return nil
}

func (q *Quotes) UnInit() {

}

func (q *Quotes) Name() string {
	return "quotes"
}

func Reg(quote Quote) {
	quotes.Reg(quote)
}

func (q *Quotes) Reg(quote Quote) {
	q.lib[quote.Name()] = quote
}

func UnReg(quote Quote) {
	quotes.UnReg(quote)
}

func (q *Quotes) UnReg(quote Quote) {
	delete(q.lib, quote.Name())
}

func NewTick(quoteName string, symbol string) (chan *Item, uint64, error) {
	return quotes.NewTick(quoteName, symbol)
}

// NewTick 按 TickerInterval tick 发送行情
func (q *Quotes) NewTick(quoteName string, symbol string) (chan *Item, uint64, error) {
	mgr, ok := q.mgr[quoteName]
	if !ok {
		return nil, 0, ErrNotFoundExchange
	}

	return mgr.newTick(symbol)
}

func DelTick(quoteName string, symbol string, id uint64) {
	quotes.DelTick(quoteName, symbol, id)
}

func (q *Quotes) DelTick(quoteName string, symbol string, id uint64) {
	mgr, ok := q.mgr[quoteName]
	if !ok {
		return
	}

	mgr.delTick(symbol, id)
}

func NewAgg(quoteName string, symbol string) (chan *Item, uint64, error) {
	return quotes.NewAgg(quoteName, symbol)
}

// NewAgg 按归集数据发送行情
func (q *Quotes) NewAgg(quoteName string, symbol string) (chan *Item, uint64, error) {
	mgr, ok := q.mgr[quoteName]
	if !ok {
		return nil, 0, ErrNotFoundExchange
	}

	return mgr.newAgg(symbol)
}

func DelAgg(quoteName string, symbol string, id uint64) {
	quotes.DelAgg(quoteName, symbol, id)
}

func (q *Quotes) DelAgg(quoteName string, symbol string, id uint64) {
	mgr, ok := q.mgr[quoteName]
	if !ok {
		return
	}

	mgr.delAgg(symbol, id)
}

func SubTimeline(quoteName string, symbol string, from, to time.Time) (*Items, error) {
	return quotes.SubTimeline(quoteName, symbol, from, to)
}

// SubTimeline 指定时间内(误差1秒)的 进 出 高 低 价格
func (q *Quotes) SubTimeline(quoteName string, symbol string, from, to time.Time) (*Items, error) {
	mgr, ok := q.mgr[quoteName]
	if !ok {
		return nil, ErrNotFoundExchange
	}

	return mgr.subTimeline(symbol, from, to)
}

func TimeLine(quoteName string, symbol string, from, to time.Time, interval time.Duration) ([]*Items, error) {
	return quotes.TimeLine(quoteName, symbol, from, to, interval)
}

// TimeLine 分时线, 指定时间段, 指定间隔
func (q *Quotes) TimeLine(quoteName string, symbol string, from, to time.Time, interval time.Duration) ([]*Items, error) {
	mgr, ok := q.mgr[quoteName]
	if !ok {
		return nil, ErrNotFoundExchange
	}

	return mgr.timeline(symbol, from, to, interval)
}

func LatestItem(quoteName, symbol string) (*Item, error) {
	return quotes.LatestItem(quoteName, symbol)
}

func (q *Quotes) LatestItem(quoteName, symbol string) (*Item, error) {
	mgr, ok := q.mgr[quoteName]
	if !ok {
		return nil, ErrNotFoundExchange
	}

	return mgr.latestItem(symbol)
}

// Restart 行情重新开始,仅限于回测数据!
func Restart(quoteName string) error {
	return quotes.Restart(quoteName)
}

func (q *Quotes) Restart(quoteName string) error {
	mgr, ok := q.mgr[quoteName]
	if !ok {
		return ErrNotFoundExchange
	}

	go mgr.run()
	return nil
}
