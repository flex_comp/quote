package quote

import (
	"errors"
	"time"
)

type Quote interface {
	Name() string
	Run(...interface{}) chan *Item
	Sub(string) error
	UnSub(string) error
}

type Item struct {
	Name   string    // 行情名称
	Symbol string    // 合约名称
	Price  float64   // 价格
	Time   time.Time // 成交时间
}

type Items struct {
	In   *Item // 进
	Out  *Item // 出
	High *Item // 高
	Low  *Item // 低
}

var (
	ErrNotFoundExchange   = errors.New("未找到交易所")
	ErrNotSubscribeSymbol = errors.New("为订阅指定合约")
	ErrSubRepeated        = errors.New("指定 交易所,合约 重复订阅")
	ErrOrderBookEmpty     = errors.New("指定 交易所,合约 订单簿为空")
	ErrNotFoundConf       = errors.New("未找到交易所配置")
)

var (
	TickerInterval     = time.Millisecond * 100
	MaxOrderBookLength = 1024 * 1024
)
