package quote

import (
	"container/list"
	"sync"
	"sync/atomic"
	"time"
)

func newSymbol(e, s string) *symbol {
	sym := &symbol{
		exchangeName: e,
		symbolName:   s,
		tick:         make(map[uint64]chan *Item),
		agg:          make(map[uint64]chan *Item),
		orderBook:    list.New(),
		orderBookIdx: make(map[time.Time]*list.Element),
	}

	go sym.run()
	return sym
}

type symbol struct {
	exchangeName string
	symbolName   string
	tick         map[uint64]chan *Item
	agg          map[uint64]chan *Item

	counter uint64

	orderBook    *list.List
	orderBookIdx map[time.Time]*list.Element

	latestUpdate             time.Time
	mtxOrderBook, mtxWatcher sync.RWMutex
}

func (s *symbol) newTick() (chan *Item, uint64, error) {
	ch := make(chan *Item, 1)
	id := atomic.AddUint64(&s.counter, 1)

	s.mtxWatcher.Lock()
	defer s.mtxWatcher.Unlock()

	s.tick[id] = ch
	return ch, id, nil
}

func (s *symbol) delTick(id uint64) {
	s.mtxWatcher.Lock()
	defer s.mtxWatcher.Unlock()

	delete(s.tick, id)
}

func (s *symbol) newAgg() (chan *Item, uint64, error) {
	ch := make(chan *Item, 1)
	id := atomic.AddUint64(&s.counter, 1)

	s.mtxWatcher.Lock()
	defer s.mtxWatcher.Unlock()

	s.agg[id] = ch
	return ch, id, nil
}

func (s *symbol) delAgg(id uint64) {
	s.mtxWatcher.Lock()
	defer s.mtxWatcher.Unlock()

	delete(s.agg, id)
}

func (s *symbol) unSafeSubTimeline(from time.Time, to time.Time) (*Items, error) {
	if from.After(to) {
		return nil, ErrOrderBookEmpty
	}

	if s.latestUpdate.IsZero() {
		return nil, ErrOrderBookEmpty
	}

	from = from.Round(TickerInterval)
	to = to.Round(TickerInterval)

	if from.After(s.latestUpdate) {
		return nil, ErrOrderBookEmpty
	}

	if to.After(s.latestUpdate) {
		to = s.latestUpdate.Round(TickerInterval)
	}

	// 如果行情缺失，找最近的开始元素
	realFrom := from
	for ; to.After(realFrom); realFrom = realFrom.Add(TickerInterval) {
		_, ok := s.orderBookIdx[realFrom]
		if ok {
			break
		}
	}

	realTo := to
	for ; from.Before(realTo); realTo = realTo.Add(-TickerInterval) {
		_, ok := s.orderBookIdx[realTo]
		if ok {
			break
		}
	}

	items := new(Items)
	fromEle, toEle := s.orderBookIdx[realFrom], s.orderBookIdx[realTo]

	items.In = fromEle.Value.(*Item)
	items.Out = toEle.Value.(*Item)

	for e := fromEle; ; e = e.Next() {
		item := e.Value.(*Item)
		if items.High == nil || items.High.Price < item.Price {
			items.High = item
		}

		if items.Low == nil || items.Low.Price > item.Price {
			items.Low = item
		}

		if e == toEle {
			break
		}
	}

	return items, nil
}

func (s *symbol) subTimeline(from time.Time, to time.Time) (*Items, error) {
	s.mtxOrderBook.RLock()
	defer s.mtxOrderBook.RUnlock()

	return s.unSafeSubTimeline(from, to)
}

func (s *symbol) timeline(from time.Time, to time.Time, interval time.Duration) ([]*Items, error) {
	if from.After(to) {
		return nil, ErrOrderBookEmpty
	}

	s.mtxOrderBook.RLock()
	defer s.mtxOrderBook.RUnlock()

	var mas []*Items
	for t := from; t.Before(to); t = t.Add(interval) {
		item, err := s.subTimeline(t, t.Add(interval))
		if err != nil {
			mas = append(mas, nil)
			continue
		}

		mas = append(mas, item)
	}

	return mas, nil
}

func (s *symbol) latestItem() (*Item, error) {
	if s.orderBook.Len() == 0 {
		return nil, ErrOrderBookEmpty
	}

	return s.orderBook.Back().Value.(*Item), nil
}

func (s *symbol) newQuote(item *Item) {
	s.mtxOrderBook.Lock()
	defer s.mtxOrderBook.Unlock()

	s.mtxWatcher.RLock()
	for _, items := range s.agg {
		items <- item
	}
	s.mtxWatcher.RUnlock()

	e := s.orderBook.PushBack(item)
	if s.orderBook.Len() > MaxOrderBookLength {
		f := s.orderBook.Remove(s.orderBook.Front())
		delete(s.orderBookIdx, f.(*Item).Time)
	}

	if s.latestUpdate.Round(TickerInterval) == item.Time.Round(TickerInterval) {
		return
	}

	s.latestUpdate = item.Time
	s.orderBookIdx[s.latestUpdate.Round(TickerInterval)] = e
}

func (s *symbol) run() {
	ticker := time.NewTicker(TickerInterval)
	defer ticker.Stop()
	for range ticker.C {
		s.mtxOrderBook.RLock()

		if s.tick == nil ||
			s.latestUpdate.IsZero() ||
			s.orderBook.Len() == 0 {

			s.mtxOrderBook.RUnlock()
			continue
		}

		s.mtxWatcher.RLock()
		for _, items := range s.tick {
			items <- s.orderBook.Back().Value.(*Item)
		}
		s.mtxWatcher.RUnlock()

		s.mtxOrderBook.RUnlock()
	}
}
